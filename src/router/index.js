import Vue from 'vue'
import VueRouter from 'vue-router'
import landing from '../views/landing.vue'
import joke from '../views/joke.vue'
import favcities from '../views/favcities.vue'
import contact3 from '../views/contact3.vue'
import contact2 from '../views/contact2.vue'
import contact from '../views/contact.vue'
import sound2 from '../views/sound2.vue'
import mcode from '../views/mcode.vue'
import greatminds from '../views/greatminds.vue'
import navbar from '../components/navbar.vue'
import cube from '../views/cube.vue'
import snow from '../views/snow.vue'
import budget from '../views/budget.vue'
import football from '../views/football.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'landing',
    component: landing
  },
  {
    path: '/joke',
    name: 'joke',
    component: joke
  },
  {
    path: '/favcities',
    name: 'favcities',
    component: favcities
  },
  {
    path: '/contact2',
    name: 'contact2',
    component: contact2
  },
  {
    path: '/contact3',
    name: 'contact3',
    component: contact3
  },
  {
    path: '/contact',
    name: 'contact',
    component: contact
  },
  {
    path: '/sound2',
    name: 'sound2',
    component: sound2
  },
  {
    path: '/mcode',
    name: 'mcode',
    component: mcode
  },
  {
    path: '/greatminds',
    name: 'greatminds',
    component: greatminds
  },
  {
    path: '/navbar',
    name: 'navbar',
    component: navbar
  },
  {
    path: '/cube',
    name: 'cube',
    component: cube
  },
  {
    path: '/snow',
    name: 'snow',
    component: snow
  },
  {
    path: '/budget',
    name: 'budget',
    component: budget
  },
  {
    path: '/football',
    name: 'football',
    component: football
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
