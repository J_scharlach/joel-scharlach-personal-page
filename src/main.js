import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import axios from 'axios'
import dadJokes from '@mikemcbride/dad-jokes'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbvue/lib/css/mdb.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'




// Vue.use(bootstrap)
Vue.config.productionTip = false

Vue.use(BootstrapVueIcons)
Vue.use(BootstrapVue)
Vue.use(axios)
Vue.use(dadJokes)



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')



